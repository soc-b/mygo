package main

// interface is a user defined type where we can define the set of abstract methods

import "fmt"

type englishBot struct{}
type dzongkhaBot struct{}

type noob interface {
	greet() string
}

func main() {
	eb := englishBot{}
	db := dzongkhaBot{}

	printGreeting(eb)
	printGreeting(db)

	// Quiz
	var storing int
	for a, b := 1, 1; a<10; a++ {
		storing = b
		b++
	}
	fmt.Println(storing)
}

func (englishBot) greet() string {
	return "hello"
}
func (dzongkhaBot) greet() string {
	return "Kuzu Zangpo la"
}

//function using the type interface where it works for the both type eb & db on greeting
func printGreeting (n noob) {

	fmt.Println(n.greet())
}

//printGreeting now returns the noob type now...
// it is due to the greet method that has become the member of noob interface where the greet methods includes the structs of englisgBot and dzongkhaBot
// or due to db & eb associated with greet methods which is the member method of the noob interface type

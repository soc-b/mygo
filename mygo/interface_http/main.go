package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	resp, err := http.Get("http://google.com")

	if err != nil {
		// Error handling code
		fmt.Println("Error", err)
		// to terminate the program
		os.Exit(1)
	}
	// fmt.Println(resp)

	bs := make([]byte, 99999) // 99999 empty elementd
	resp.Body.Read(bs)

	fmt.Println(string(bs))

	// io.Copy(os.Stdout, resp.Body) // shorthand for reading the body of the Get function
}
package main

import "fmt"

//mapName := map[key type]valueType{} ---------Signature of declaring a map

func main() {
	boy:= map[string]bool{
		"Handsome": false,
		"Gentle": false,
		"stupid": true,
	}
	delete(boy, "Gentle")
	fmt.Println(boy)

	colors := map[string]string {
		"red": "danger",
		"green": "nature",
		"blue": "sky",
	}
	printMap(colors)

	// other way to create a map
	//var colors map[string]string
	//colors := make(map[string]string)

	// assigning the value to an empty map and it is also used to update the existing key's value
	//colors["green"] = "live"

	//how to delete key value pair in map
	//syntax====== delete(colors, "green")----- why key only ==== key have to be unique in map where the valjue have no restriction on it.....


}

//iterating the map 
func printMap(c map[string]string) {
	for c, def := range c {
		println(" Definition of "+ c + " is " + def+ ". ")
	}
}

package main

import "fmt"

type contactInfo struct{
	email string
	mobile int
}

type person struct {
	firstname string
	lastname string
	age int
	married bool
	address contactInfo
}

func main() {
	// person1:= person{"Kuenga", "Tshering", 24, true}
	// person2:= person{firstname: "Phurpa", lastname: "Dorji", age: 20, married: false}

	// // empty object or struct
	// myname:= person{}

	// // to add on empty object ... we will use dot operator
	// myname.firstname="Sangay"
	// myname.lastname="Thinley"
	// myname.age=34
	// myname.married=false

	// fmt.Println(person1, person2)
	// // fmt.Printf("%+v", person1)
	// fmt.Println(myname)

	//// more advance feature of structs
	//// two objects or struct is embedded

	Phurpa := person{
		firstname: "Phurpa",
		lastname: "Dorji",
		age: 20,
		married: false,
		address: contactInfo{
			email: "phurpadorji@gmail.com",
			mobile: 17984989,
		},
	}

	// Sangay := person{
	// 	firstname: "Sangay",
	// 	lastname: "Dorji",
	// 	age: 59,
	// 	married: true,
	// 	contactInfo:= {
	// 		email: "phurpadorji@gmail.com",
	// 		mobile: 17984989,
	// 	},
	// }

	// fmt.Printf("%+v", Phurpa)
	// PhurpaPointer :=&Phurpa
	// PhurpaPointer.updateName("James")
	// Phurpa.print()
	
	//Sharthand form of * and & +
	Phurpa.updateName("Sonam")
	fmt.Println(Phurpa)

}

func (p person) print() {
	fmt.Printf("%+v", p)
}

// Structs with pointers---pointer is a variable that stores memory address of another variable 
func (p *person) updateName(newFirstName string) {
	(*p).firstname = newFirstName
}

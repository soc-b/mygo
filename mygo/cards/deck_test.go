package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()
	if len(d) != 52 {
		t.Errorf("Expected deck length of 52 but got %d", len(d))
	}
	// test 2
	if d[0] != "ace of Spades" {
		t.Errorf("Expected ace of Spades but found %v", d[0])
	}

	// test 3
	if d[len(d)-1] != "king of Heart" {
		t.Errorf("Expected king of Heart but found %v", d[len(d)-1])
	}

}

func TestSaveToFileAndNewDeckFromFile(t *testing.T){
	de :=newDeck()
	os.Remove("_decktesting")
	de.saveToFile("_decktesting")

	deck := newDeckFromFile("_decktesting")
	if len(deck) != 52 {
		t.Errorf("The length of the deck is not 24, it is %v",len(deck))
	}
	os.Remove("_decktesting")
}



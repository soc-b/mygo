package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
)

type deck []string

//to print list of cards
// it haas a reciever deck so therefore it should be printed using the instances of the deck type
// reciever function ---
func (d deck) print() {
	for i, v := range d {
		fmt.Println(i, v)
	}
}


// creating a newDeck
func newDeck() deck{
	cards:=deck{} //empty deck
	cardSuits :=[]string{"Spades","Club", "Diamond", "Heart"}
	cardValue := []string{"ace", "two", "three", "four", "five", "six","seven", "eight","nine", "ten", "jerk", "queen", "king"}

	for _, s :=range cardSuits{
		for _, v := range cardValue{
			cards = append(cards, v+" of "+s)
		}
	}
	return cards

}

// normal function
// func deal(d deck, handsize int) (deck, deck) {
// 	cardsInHand := d[:handsize]
// 	remainingCards := d[handsize:]
// 	return cardsInHand, remainingCards
// }

// reciever type function
func (d deck)deal(handsize int) (deck, deck) {
	cardsInHand := d[:handsize]
	remainingCards := d[handsize:]
	return cardsInHand, remainingCards
}

// helper function to convert
func (d deck) toString() string{
	return strings.Join([]string(d), ",")
	
}

func (d deck)saveToFile(fileName string) error{
	str := d.toString()
	// writefile
	err:= os.WriteFile(fileName, []byte(str), 0666)
	return err

}


func toDeck(fileName string)deck{
	s := strings.SplitAfter(string(fileName), ",")
	return deck(s)

}

//read the file using function ReadFile of os package
func newDeckFromFile(fileName string)deck {
	byteSlice, err := os.ReadFile(fileName)
	if err != nil{
		fmt.Println("error:", err)
		os.Exit(1)
	}
	return toDeck(string(byteSlice))
}


func (d deck)shuffle() deck{
	for i := range d {
		newIndex := rand.Intn(len(d)-1)

		d[i], d[newIndex] = d[newIndex], d[i]
	}
	return d
}







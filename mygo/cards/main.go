package main

import "fmt"

// import "fmt"

func main() {
	// fmt.Println("Hello World")
	// help()
	// mypackage.Pack()
	// card := newcard()
	// fmt.Println(card)
	// card := []string{"ace of diamonds", "two of heart", "one of club", newcard()}
	// fmt.Println(card)
	// var s []byte
	// s = make([]byte, 5, 5)
	// fmt.Println(s)
	// b := []string{"g", "o", "l", "a", "n", "g"}
	// d := b[1:4]
	// fmt.Println(d)
	// cards := []string{"ace of diamond", newcard()}
	// cards = append(cards, "six of spades", "hgfgshgufgbhsdg", "yusgfugfhubuf")
	// fmt.Println(cards)

	// for i, v := range cards {
	// 	fmt.Println(i, v)

	// }
	// to ignore the in index
	// for _, v := range cards {
	// 	fmt.Println(v)

	// }
	// //  to ignore the value
	// for i := range cards {
	// 	fmt.Println(i)

	// }

	// cards := deck{"ace of diamaond", newcard()}
	// cards = append(cards, "six of spades")
	// fmt.Println(cards)

	//since this cards is deck type so we can access or called the function of type deck where print belongs to deck type 
	// cards.print()

	// invoking of the function
	dec := newDeck()
	dec = dec.shuffle()
	dec.print()

	// pema, thinley := deal(dec, 8)
	// fmt.Println("Cards in hand")
	// pema.print()
	// fmt.Println("Remaining cards left in the Deck!")
	// thinley.print()

	// cardsInHanaad, remainingCards:=deal(dec, 5)
	// fmt.Println("Cards in hand")
	// cardsInHanaad.print()
	// fmt.Println("Remaining cards left in the Deck!")
	// remainingCards.print()

	// reciever type function
	// cardsInHanaad, remainingCards:=dec.deal(5)
	// fmt.Println("Cards in hand")
	// cardsInHanaad.print()
	// fmt.Println("Remaining cards left in the Deck!")
	// remainingCards.print()
	
	
	// fmt.Println(dec.toString())//the whole cards is now converted into a single string

	dec.saveToFile("my_File")

	// quiz
	var num int64 = 100

    n := &num

    fmt.Printf("%p\n", n)

    fmt.Println(n)  

    fmt.Printf("%p\n", &n)
	

}

// label return type
// func newcard() (c string) {
// 	c = "ace of spade"
// 	return
// }
